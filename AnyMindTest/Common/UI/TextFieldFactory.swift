//
//  TextFieldFactory.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class TextFieldFactory {
    
    weak var delegate: UITextFieldDelegate?
    
    init(delegate: UITextFieldDelegate? = nil) {
        self.delegate = delegate
    }
    
    func makeForPhone(placeholder: String?) -> UITextField {
        let field = UITextField()
        field.borderStyle = .roundedRect
        field.placeholder = placeholder
        field.keyboardType = .phonePad
        field.delegate = self.delegate
        return field
    }
    
    func makeForEmail(placeholder: String?) -> UITextField {
        let field = UITextField()
        field.borderStyle = .roundedRect
        field.placeholder = placeholder
        field.keyboardType = .emailAddress
        field.delegate = self.delegate
        return field
    }
    
    func makeDefault(placeholder: String?) -> UITextField {
        let field = UITextField()
        field.borderStyle = .roundedRect
        field.placeholder = placeholder
        field.delegate = self.delegate
        return field
    }
}
