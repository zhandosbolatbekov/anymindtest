//
//  AppCoordinator.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    private let navigationController: UINavigationController
    
    private var startCoordinator: Coordinator?
    
    init(window: UIWindow) {
        self.window = window
        self.navigationController = UINavigationController()
        self.window.rootViewController = self.navigationController
        self.window.makeKeyAndVisible()
        
        self.startCoordinator = ResumeFlowCoordinator(navigationController: self.navigationController)
    }
    
    func start() {
        self.startCoordinator?.start()
    }
}

