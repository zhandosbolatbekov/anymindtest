//
//  Coordinator.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

protocol Coordinator {

    func start()
}
