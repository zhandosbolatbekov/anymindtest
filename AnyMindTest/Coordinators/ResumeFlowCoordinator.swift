//
//  ResumeListFlowCoordinator.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class ResumeFlowCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let controller = ResumeModuleBuilder.build()
        self.navigationController.setViewControllers([controller], animated: false)
    }
}
