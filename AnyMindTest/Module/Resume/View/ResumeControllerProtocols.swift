//
//  ResumeControllerProtocols.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

protocol ResumeControllerInput: AnyObject {
    func fill(with viewAdapter: ResumeViewAdapter)
    
    func getCurrentState() -> ResumeViewAdapter
    func showResetWarningAlert()
}

protocol ResumeControllerOutput: AnyObject {
    
    func viewIsReady()
    
    func didTapAddNew()
    func didTapSave()
    
    func removeStoredResume()
}
