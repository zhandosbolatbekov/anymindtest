//
//  ResumeViewController.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class ResumeViewController: UIViewController, ResumeControllerInput {
    
    private let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.alwaysBounceVertical = true
        scroll.showsVerticalScrollIndicator = false
        scroll.keyboardDismissMode = .interactive
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    private let content: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imagePicker = ImagePicker(presentationController: self, delegate: self)
    
    private lazy var photoView = ResumePhotoView(delegate: self)
    
    private let textFieldFactory = TextFieldFactory()
    private lazy var phoneField = self.textFieldFactory.makeForPhone(placeholder: "Mobile number")
    private lazy var emailField = self.textFieldFactory.makeForEmail(placeholder: "Email Address")
    private lazy var residenceField = self.textFieldFactory.makeDefault(placeholder: "Residence Address")
    private lazy var careerField = self.textFieldFactory.makeDefault(placeholder: "Career Objective")
    private lazy var experienceView = ExperienceYearsView()
    
    private let output: ResumeControllerOutput
    
    init(output: ResumeControllerOutput) {
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Life-cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        self.output.viewIsReady()
    }
    
    // MARK: - ResumeControllerInput
    
    func fill(with viewAdapter: ResumeViewAdapter) {
        if let imageData = viewAdapter.image {
            self.photoView.set(image: UIImage(data: imageData))
        }
        self.phoneField.text = viewAdapter.phoneNumber
        self.emailField.text = viewAdapter.email
        self.residenceField.text = viewAdapter.residence
        self.careerField.text = viewAdapter.objective
        self.experienceView.select(row: viewAdapter.experienceYears ?? 0)
    }
    
    func getCurrentState() -> ResumeViewAdapter {
        .init(image: self.photoView.currentImage()?.pngData(),
              phoneNumber: self.phoneField.text,
              email: self.emailField.text,
              residence: self.residenceField.text,
              objective: self.careerField.text,
              experienceYears: self.experienceView.currentSelected())
    }
    
    func showResetWarningAlert() {
        let alert = UIAlertController(title: "You are creating a new resume, which causes removing the previous one",
                                      message: "Are you sure you want to do that?",
                                      preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { [weak self] _ in
            self?.output.removeStoredResume()
        }
        alert.addAction(confirmAction)
        
        self.present(alert, animated: true)
    }
    
    // MARK: - Private methods
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.navigationItem.title = "Resume"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.didTapAddButton))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(self.didTapSaveButton))
        
        self.view.addSubview(self.scrollView)
        
        var constraints = [NSLayoutConstraint]()
        constraints.append(contentsOf: [
            self.scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.scrollView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.scrollView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            self.scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.scrollView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        ])
        
        self.scrollView.addSubview(self.content)
        let contentHeightConstraint = self.content.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor)
        contentHeightConstraint.priority = .init(rawValue: 1)
        
        constraints.append(contentsOf: [
            self.content.topAnchor.constraint(equalTo: self.scrollView.topAnchor),
            self.content.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor),
            self.content.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor),
            self.content.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor),
            self.content.widthAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.widthAnchor),
            contentHeightConstraint
        ])
        
        let stack = UIStackView(arrangedSubviews: [photoView, phoneField, emailField, residenceField, careerField, experienceView])
        stack.axis = .vertical
        stack.spacing = 16
        stack.distribution = .equalSpacing
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        self.content.addSubview(stack)
        constraints.append(contentsOf: [
            stack.topAnchor.constraint(equalTo: self.content.topAnchor, constant: 16),
            stack.leadingAnchor.constraint(equalTo: self.content.leadingAnchor, constant: 16),
            stack.trailingAnchor.constraint(equalTo: self.content.trailingAnchor, constant: -16),
            stack.bottomAnchor.constraint(equalTo: self.content.bottomAnchor, constant: -16),
        ])
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc
    private func didTapAddButton() {
        self.output.didTapAddNew()
    }
    
    @objc
    private func didTapSaveButton() {
        self.output.didTapSave()
    }
}

extension ResumeViewController: ResumePhotoViewDelegate {
    
    func editPhoto() {
        self.imagePicker.present(from: self.photoView)
    }
}

extension ResumeViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.photoView.set(image: image)
    }
}
