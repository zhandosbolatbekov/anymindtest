//
//  ResumePresenter.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

final class ResumePresenter: ResumeControllerOutput {
    
    weak var viewInput: ResumeControllerInput?
    private let repository: ResumeRepository
    
    init(repository: ResumeRepository) {
        self.repository = repository
    }
    
    // MARK: - ResumeControllerOutput
    
    func viewIsReady() {
        if let adapter = self.buildViewAdapter() {
            self.viewInput?.fill(with: adapter)
        }
    }
    
    func didTapAddNew() {
        self.viewInput?.showResetWarningAlert()
    }
    
    func didTapSave() {
        if let state = self.viewInput?.getCurrentState() {
            self.repository.save(resume: self.buildResume(from: state))
        }
    }
    
    func removeStoredResume() {
        self.repository.save(resume: nil)
        self.viewInput?.fill(with: .empty())
    }
    
    // MARK: - Private methods
    
    private func buildViewAdapter() -> ResumeViewAdapter? {
        
        guard let saved = self.repository.getResume() else {
            return nil
        }
        return ResumeViewAdapter(image: saved.image,
                                 phoneNumber: saved.phoneNumber,
                                 email: saved.email,
                                 residence: saved.residenceAddress,
                                 objective: saved.careerObjective,
                                 experienceYears: saved.totalYearsOfExp)
    }
    
    private func buildResume(from adapter: ResumeViewAdapter) -> Resume {
        .init(image: adapter.image,
              phoneNumber: adapter.phoneNumber,
              email: adapter.email,
              residenceAddress: adapter.residence,
              careerObjective: adapter.objective,
              totalYearsOfExp: adapter.experienceYears)
    }
}
