//
//  ResumeViewAdapter.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

struct ResumeViewAdapter {
    
    static func empty() -> ResumeViewAdapter {
        .init(image: nil, phoneNumber: nil, email: nil, residence: nil, objective: nil, experienceYears: nil)
    }
    
    var image: Data?
    var phoneNumber: String?
    var email: String?
    var residence: String?
    var objective: String?
    var experienceYears: Int?
}
