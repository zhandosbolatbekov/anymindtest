//
//  ResumePhotoView.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 10.03.2022.
//

import UIKit

protocol ResumePhotoViewDelegate: AnyObject {
    func editPhoto()
}

final class ResumePhotoView: UIView {
    
    private enum Constants {
        static let avatarSize: CGFloat = 100
    }
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = Constants.avatarSize / 2
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.clipsToBounds = true
        return view
    }()
    
    weak var delegate: ResumePhotoViewDelegate?
    
    init(delegate: ResumePhotoViewDelegate? = nil) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.configureUI()
    }
    
    required init?(coder: NSCoder) {
        nil
    }
    
    func currentImage() -> UIImage? {
        self.imageView.image
    }
    
    func set(image: UIImage?) {
        self.imageView.image = image
    }
    
    private func configureUI() {
        self.addSubview(self.imageView)
        
        let editButton = self.createEditButton()
        self.addSubview(editButton)
        
        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            self.imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
            self.imageView.widthAnchor.constraint(equalToConstant: Constants.avatarSize),
            self.imageView.heightAnchor.constraint(equalToConstant: Constants.avatarSize),
            
            editButton.leadingAnchor.constraint(equalTo: self.imageView.trailingAnchor, constant: 16),
            editButton.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor)
        ])
    }
    
    private func createEditButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(UIColor.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(self.didTapEdit), for: .touchUpInside)
        return button
    }
    
    @objc
    private func didTapEdit() {
        self.delegate?.editPhoto()
    }
}
