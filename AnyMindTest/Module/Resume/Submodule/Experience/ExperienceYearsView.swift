//
//  ExperienceYearsView.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class ExperienceYearsView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private enum Constants {
        static let numberOfYearsLimit = 100
    }
    
    private lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.configureUI()
    }
    
    required init?(coder: NSCoder) {
        nil
    }
    
    func currentSelected() -> Int {
        self.pickerView.selectedRow(inComponent: 0)
    }
    
    func select(row: Int) {
        self.pickerView.selectRow(row, inComponent: 0, animated: true)
    }
    
    private func configureUI() {
        
        let label = LabelFactory().makeSectionTitle(text: "Total Years of experience")
        self.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        
        self.addSubview(self.pickerView)
        NSLayoutConstraint.activate([
            self.pickerView.topAnchor.constraint(equalTo: label.bottomAnchor),
            self.pickerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.pickerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.pickerView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        Constants.numberOfYearsLimit
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        "\(row)"
    }
}
