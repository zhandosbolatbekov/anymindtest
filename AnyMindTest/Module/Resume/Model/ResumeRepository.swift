//
//  ResumeRepository.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

protocol ResumeRepository {
    
    func getResume() -> Resume?
    
    @discardableResult
    func save(resume: Resume?) -> Bool
}


