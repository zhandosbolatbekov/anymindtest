//
//  ResumeRepositoryImpl.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

final class ResumeRepositoryImpl: ResumeRepository {
    
    private enum UserDefaultsKey {
        static let myResume = "MyResume"
    }
    
    private let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func getResume() -> Resume? {
        guard let resumeData = self.userDefaults.object(forKey: UserDefaultsKey.myResume) as? Data else {
            return nil
        }
        let decoder = JSONDecoder()
        return try? decoder.decode(Resume.self, from: resumeData)
    }
    
    @discardableResult
    func save(resume: Resume?) -> Bool {
        guard let resume = resume else {
            self.userDefaults.removeObject(forKey: UserDefaultsKey.myResume)
            return true
        }
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(resume) {
            self.userDefaults.set(encoded, forKey: UserDefaultsKey.myResume)
            return true
        }
        return false
    }
}
