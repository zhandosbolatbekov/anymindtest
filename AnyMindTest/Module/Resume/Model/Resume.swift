//
//  Resume.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import Foundation

struct Resume: Codable {
    
    struct WorkItem: Codable {
        
        var companyName: String?
        var duration: String?
    }
    
    struct EducationItem: Codable {
        
        var `class`: String?
        var passingYear: String?
        var percentage: String?
    }

    struct ProjectItem: Codable {
        
        var name: String?
        var teamSize: String?
        var projectSummary: String?
        var technologiesUsed: String?
        var role: String?
    }
    
    var image: Data?
    var phoneNumber: String?
    var email: String?
    var residenceAddress: String?
    var careerObjective: String?
    var totalYearsOfExp: Int?
    var workSummary: [WorkItem] = []
    var skills: [String] = []
    var educationDetails: [EducationItem] = []
    var projectDetails: [ProjectItem] = []
}


