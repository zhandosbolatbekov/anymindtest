//
//  ResumeModuleBuilder.swift
//  AnyMindTest
//
//  Created by Zhandos Bolatbekov on 09.03.2022.
//

import UIKit

final class ResumeModuleBuilder {
    
    static func build() -> UIViewController {
        
        let repository = ResumeRepositoryImpl(userDefaults: .standard)
        let presenter = ResumePresenter(repository: repository)
        let controller = ResumeViewController(output: presenter)
        presenter.viewInput = controller
        return controller
    }
}
